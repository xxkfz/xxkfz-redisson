package cn.bugstack.xfg.dev.tech.domain.order.service;

import cn.bugstack.xfg.dev.tech.domain.order.model.valobj.MessageVO;
import com.lmax.disruptor.RingBuffer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public interface DisruptorMqService {


    /**
     * 消息
     *
     * @param message
     */
    void sayHelloMq(String message);
}


