package cn.bugstack.xfg.dev.tech.domain.order.service;

import cn.bugstack.xfg.dev.tech.domain.order.model.aggregate.OrderAggregate;
import cn.bugstack.xfg.dev.tech.domain.order.model.entity.OrderEntity;
import cn.bugstack.xfg.dev.tech.domain.order.model.entity.SKUEntity;
import cn.bugstack.xfg.dev.tech.domain.order.model.entity.UserEntity;
import cn.bugstack.xfg.dev.tech.domain.order.model.valobj.DeviceVO;
import cn.bugstack.xfg.dev.tech.domain.order.repository.IOrderRepository;
import cn.hutool.core.lang.UUID;
import com.xxkfz.simplememory.annotation.AsyncExec;
import com.xxkfz.simplememory.enums.AsyncTypeEnum;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.List;

/**
 * @author Fuzhengwei bugstack.cn @小傅哥
 * @description 订单服务
 * @create 2023-09-03 15:01
 */
@Service
@Slf4j
public class OrderService implements IOrderService {

    @Resource
    private IOrderRepository orderRepository;


    @Override
    public long initSkuCount(String sku, long count) {
        return orderRepository.initSkuCount(sku, count);
    }

    @Override
    public String createOrderByNoLock(OrderAggregate orderAggregate) {
        return orderRepository.createOrderByNoLock(orderAggregate);
    }

    @Override
    public String createOrderByLock(OrderAggregate orderAggregate) {
        return orderRepository.createOrderByLock(orderAggregate);
    }

    @Override
    public String createOrder(OrderAggregate orderAggregate) {
        return orderRepository.createOrder(orderAggregate);
    }

    @Override
    public OrderEntity queryOrder(String orderId) {
        return orderRepository.queryOrder(orderId);
    }

    @Override
    public String payOrder(String orderId) {
        return orderRepository.payOrder(orderId);
    }

    @Override
    public List<OrderEntity> list(int pageNo, int pageSize) {
        return orderRepository.list(pageNo, pageSize);
    }

    @Override
    @AsyncExec(type = AsyncTypeEnum.SAVE_ASYNC, remark = "测试1")
    public void test() {
        String sku = RandomStringUtils.randomNumeric(9);
        log.error("执行异步方法......");
        String uuid = UUID.fastUUID().toString();

        UserEntity userEntity = UserEntity.builder().userId(uuid).userName("小小开发者".concat(RandomStringUtils.randomNumeric(3))).userMobile("+86 13521408***").build();

        SKUEntity skuEntity = SKUEntity.builder().sku(sku).skuName("《手写MyBatis：渐进式源码实践》").quantity(1).unitPrice(BigDecimal.valueOf(128)).discountAmount(BigDecimal.valueOf(50)).tax(BigDecimal.ZERO).totalAmount(BigDecimal.valueOf(78)).build();

        DeviceVO deviceVO = DeviceVO.builder().ipv4("127.0.0.1").ipv6("2001:0db8:85a3:0000:0000:8a2e:0370:7334".getBytes()).machine("IPhone 14 Pro").location("shanghai").build();

        this.createOrder(new OrderAggregate(userEntity, skuEntity, deviceVO));

        int i = 10/0;
    }
}
