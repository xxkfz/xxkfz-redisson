package cn.bugstack.xfg.dev.tech.domain.order.model.map;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * 获取有序map中key值的下标, 通过foreach循环put进ArrayList
 *
 * @param <K>
 * @param <V>
 */
public class IndexedLinkedHashMap<K, V> extends LinkedHashMap<K, V> {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = 1L;
    /**
     * 集合
     */
    List<K> arrayList = new ArrayList<K>();


    @Override
    public V put(K key, V val) {
        if (!super.containsKey(key)) {
            arrayList.add(key);
        }
        V returnValue = super.put(key, val);
        return returnValue;
    }

    /**
     * 根据值获取下标
     *
     * @param i
     * @return
     */
    public V getValueAtIndex(int i) {
        return (V) super.get(arrayList.get(i));
    }

    /**
     * 根据下标获取key
     *
     * @param i
     * @return
     */
    public K getKeyAtIndex(int i) {
        return (K) arrayList.get(i);
    }

    /**
     * 根据键获取下标
     *
     * @param key
     * @return
     */
    public int getIndexOf(K key) {
        return arrayList.indexOf(key);
    }

}
