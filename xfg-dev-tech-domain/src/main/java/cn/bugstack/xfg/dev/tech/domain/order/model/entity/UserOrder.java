package cn.bugstack.xfg.dev.tech.domain.order.model.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @program: xfg-dev-tech-redis-master
 * @ClassName UserOrder.java
 * @author: wusongtao
 * @create: 2023-11-01 11:08
 * @description:
 * @Version 1.0
 **/
@Setter
@Getter
@ToString
public class UserOrder {
    private String userName;

}