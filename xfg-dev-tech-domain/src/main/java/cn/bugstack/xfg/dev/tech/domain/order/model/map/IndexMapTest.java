package cn.bugstack.xfg.dev.tech.domain.order.model.map;

import cn.bugstack.xfg.dev.tech.domain.order.model.entity.UserOrder;
import org.apache.commons.lang3.math.NumberUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @program: xfg-dev-tech-redis-master
 * @ClassName IndexMapTest.java
 * @author: wusongtao
 * @create: 2023-11-01 11:01
 * @description:
 * @Version 1.0
 **/
public class IndexMapTest {
    public static Integer getStatisticRank(String key, Map<String, UserOrder> idCntMap) {
        // 根据key进行排序
        LinkedHashMap<String, UserOrder> collect = idCntMap.entrySet().stream()
                .sorted((Comparator.comparingInt(o -> Integer.valueOf(o.getKey()))))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

        IndexedLinkedHashMap<String, UserOrder> indexedLinkedHashMap = new IndexedLinkedHashMap<>();

        collect.forEach((k, v) -> {
            indexedLinkedHashMap.put(k, v);
        });
        System.out.println("indexedLinkedHashMap = " + indexedLinkedHashMap);

        int indexOf = indexedLinkedHashMap.getIndexOf(key);
        return NumberUtils.INTEGER_MINUS_ONE.equals(indexOf) ? -1 : indexOf + 1;
    }


    public static void main(String[] args) {
        Map<String, UserOrder>  map = new LinkedHashMap<>();
        map.put("200",new UserOrder());
        map.put("13",new UserOrder());
        map.put("3",new UserOrder());
        map.put("200",new UserOrder());
        map.put("13",new UserOrder());


        Integer statisticRank = getStatisticRank("13", map);
        System.out.println("statisticRank = " + statisticRank);



    }

}