package cn.bugstack.xfg.dev.tech.domain.order.handler;

import cn.bugstack.xfg.dev.tech.domain.order.model.valobj.MessageVO;
import com.lmax.disruptor.EventHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: xfg-dev-tech-redis-master
 * @ClassName HelloEventHandler.java
 * @author: wusongtao
 * @create: 2023-10-24 15:08
 * @description:
 * @Version 1.0
 **/

@Slf4j
public class HelloEventHandler implements EventHandler<MessageVO> {


    @Override
    public void onEvent(MessageVO event, long sequence, boolean endOfBatch) {


        try {

            //这里停止1000ms是为了确定消费消息是异步的
            Thread.sleep(1000);
            log.info("消费者处理消息开始");
            if (event != null) {


                log.info("消费者消费的信息是：{}", event);
            }
        } catch (Exception e) {


            log.info("消费者处理消息失败");
        }
        log.info("消费者处理消息结束");
    }
}
