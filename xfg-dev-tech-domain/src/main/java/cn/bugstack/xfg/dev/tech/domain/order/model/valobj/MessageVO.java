package cn.bugstack.xfg.dev.tech.domain.order.model.valobj;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


@Data
@Setter
@Getter
public class MessageVO {
    private String message;

}