package cn.bugstack.xfg.dev.tech.domain.order.factory;

import cn.bugstack.xfg.dev.tech.domain.order.model.valobj.MessageVO;
import com.lmax.disruptor.EventFactory;

public class HelloEventFactory implements EventFactory<MessageVO> {

    @Override
    public MessageVO newInstance() {
        return new MessageVO();
    }
}
