package cn.bugstack.xfg.dev.tech.config;

import com.github.pagehelper.PageInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @program: xxkfz-redisson
 * @description:
 * @author: wust
 * @create: 2023-10-10 21:34
 **/
@Configuration
public class PageInterceptorConfig {

    @Bean
    public PageInterceptor pageInterceptor() {
        return new PageInterceptor();
    }
}
