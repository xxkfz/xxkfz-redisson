package cn.bugstack.xfg.dev.tech.domain.controller;

import cn.bugstack.xfg.dev.tech.infrastructure.redis.IRedisService;
import cn.hutool.extra.spring.SpringUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RKeys;
import org.redisson.api.RateType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.List;

/**
 * @program: xfg-dev-tech-redis-master
 * @ClassName RedisController.java
 * @author: wusongtao
 * @create: 2023-10-31 13:53
 * @description: Redisson 基本操作
 * @Version 1.0
 **/
@RestController
@RequestMapping
@Slf4j
public class RedisController {

    @Autowired
    private IRedisService iRedisService;

    @Autowired
    RedisTemplate redisTemplate;

    @GetMapping("/redis")
    public String getKeysByPattern() {
        RKeys keys = iRedisService.getKeys();
        // 模糊配备所有的key
        Iterable<String> keysIt = keys.getKeysByPattern("performance_analy*");
        Iterator<String> iterator = keysIt.iterator();
        while (iterator.hasNext()) {
            String next = iterator.next();
            System.out.println("next = " + next);
        }
        return "200";
    }

    @GetMapping("/rate")
    public String rateLimiter(){
        long l = iRedisService.rateLimiter("xxkfz_1", RateType.OVERALL, 5, 10);
        return String.valueOf(l);

    }

}