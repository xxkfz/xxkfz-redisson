package cn.bugstack.xfg.dev.tech;

import java.util.concurrent.locks.ReentrantLock;

/**
 * @program: xfg-dev-tech-redis-master
 * @ClassName ReentranLockTest.java
 * @author: wusongtao
 * @create: 2023-11-05 10:48
 * @description:
 * @Version 1.0
 **/
public class ReentranLockTest  implements Runnable{
    ReentrantLock lock = new ReentrantLock();

    public void get() {
        //第一个断点打再这  state 0->1 由无锁转入有锁
        lock.lock();
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().getName());
        set();
        //state 由1->0 至此同一线程的锁变成无锁状态
        lock.unlock();
    }

    public void set() {
        //第二个断点打在这 state 1->2
        lock.lock();
        System.out.println(Thread.currentThread().getId() + Thread.currentThread().getName());
        //2->1
        lock.unlock();
    }

    @Override
    public void run() {
        get();
    }

    public static void main(String[] args) {
        ReentranLockTest ss = new ReentranLockTest();
        new Thread(ss).start();
        //new Thread(ss).start();
        //new Thread(ss).start();
    }
}
